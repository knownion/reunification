# Reunification

Reunification act like an hub for all projects in this group.

## Requirements 
* You need to download all repositories in the group: `Knownion`
  * parsing-doc
  * reunification
  * storage
  * upload
* You need to have `Docker` and `Docker Compose` installed

## Installation

In your terminal go to `reunification` directory :
```
cd reunification
```

Then you can start all services :

```sh
docker-compose up --build
```

## Convention

Container is accessible at `http://MACHINE_IP:5000`and accept zip file send by a form using `PUT` method with a field named `knownion_doc`.

Your zip file should at least contain you `.md` document. It can also embbed images too. 


Don't forget to add the following information at the top of your document. 
```
<!-- 
{
  keywords: [
    foo,
    bar
  ]
}
-->
```

## Note

You should see README.md from `draft` before start developping